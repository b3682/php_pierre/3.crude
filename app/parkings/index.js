
console.log("<test>");

const fs = require("fs");
const express = require("express");
const parkings = require('./parkings.json');
// const res = require("express/lib/response");
const app = express();
const port = 222;

app.use(express.json());

//home page / 
app.get('/',(req,res) => {
    res.send("HOME PAGE : nothing to do here");
});

// on recup les infos 
app.get('/infos', (req,res) => {
    res.writeHead(200, {'Content-Type':'application/json'});
    res.end(fs.readFileSync('package.json'))
})


// recup le json(parkings) au nom de l'url parkings
app.get('/parkings', (req,res) => {
    res.status(200).json(parkings);
})


//on recup l'index d'un tableau en fonction d' l'id entrée en param après parkings/
app.get('/parkings/:id', (req,res) => {
    // qu'on converti en entier
    const id = parseInt(req.params.id);
    const parking = parkings.find(parking => parking.id === id);
    if (parking) { res.status(200).json(parking);}
    else { res.status(404).end();}
    //on retourne l'élément qui se trovue à l'id entrée
    res.status(200).json(parking);
})



app.post('/parkings',(req,res) => {
    //req.body = notre element postman qu'on envoie dans parkings.json
    
    let obj = JSON.parse(fs.readFileSync("parkings.json"));
    obj.push(req.body);
    let newParking = JSON.stringify(obj);
    fs.writeFileSync("parkings.json", newParking);
    res.status(200).json(parkings);


});

app.put('/parkings/:id', (req, res) => {
    const id = parseInt(req.params.id);
    let parking = parkings.find(parking => parking.id === id);
    parking.name = req.body.name;
    parking.city = req.body.city;
    parking.type = req.body.type;
    res.status(200).json(parking);
});

app.delete('/parkings/:id', (req,res) => {
    const id = parseInt(req.params.id);
    let parking = parkings.find(parking => parking.id === id);
    parkings.splice(parkings.indexOf(parking),1);
    res.status(200).json(parkings);
})
//ecoute notre port de co
app.listen(port, () => {
    console.log("running at : " + port);
});


//>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// stockage de la db sur le pc avec sqlite3
const sqlite3 = require('sqlite3').verbose();

// open database in memory
let db = new sqlite3.Database(':memory:', (err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Connected to the in-memory SQlite database.');
});

 

// close the database connection
db.close((err) => {
  if (err) {
    return console.error(err.message);
  }
  console.log('Close the database connection.');
});